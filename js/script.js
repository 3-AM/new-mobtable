$(document).ready(function(){

	(function() {
		var mySwiper = $('.swiper-carousel').swiper({
			loop:true,
			slidesPerView: 'auto'
		});
		$('.slide-prev').on('click', function(e){
			e.preventDefault();
			var swiper = $(this).parent('.swiper-carousel').data('swiper');
			swiper.swipePrev();
		});
		$('.slide-next').on('click', function(e){
			e.preventDefault();
			var swiper = $(this).parent('.swiper-carousel').data('swiper');
			swiper.swipeNext();
		});
	})();

	(function() {
		var mySlidebars = new $.slidebars();
		$('#btn-menu').on('click', function() {
			mySlidebars.slidebars.toggle('right');
		});
	})();

	(function(){
		var tabs = $('[data-role = tabs]');

		if (tabs.length){
			var tabsBtn = tabs.find('.tabs-buttons').children();

			tabsBtn.eq(0).addClass('active');
			$('.tabs-content').children().hide();
			$('.tabs-content').children().eq(0).show();

			tabsBtn.on('click', function(e){
				var closestTabs;
				var tabsContent;

				e.preventDefault();
				
				$(this).addClass('active').siblings().removeClass('active');
				closestTabs = $(this).closest(tabs);
				tabsContent = closestTabs.find('.tabs-content').children();
				tabsContent.hide();
				tabsContent.eq($(this).index()).fadeIn(400);
			});
		}
	})();

	(function ($) {
		$.support.placeholder = ('placeholder' in document.createElement('input'));
	})(jQuery);

	$(function () {
		if (!$.support.placeholder) {
			$("[placeholder]").focus(function () {
				if ($(this).val() == $(this).attr("placeholder")) $(this).val("");
			}).blur(function () {
				if ($(this).val() == "") $(this).val($(this).attr("placeholder"));
			}).blur();

			$("[placeholder]").parents("form").submit(function () {
				$(this).find('[placeholder]').each(function() {
					if ($(this).val() == $(this).attr("placeholder")) {
						$(this).val("");
					}
				});
			});
		}
	});
});